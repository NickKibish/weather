//
//  WeatherModel.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol Mappable {
    init(json: [String : Any]?) throws
}

protocol WeatherModel: Mappable {
    var location: LocationModel { get }
    var main: MainWeatherDataModel { get }
    var wind: WindModel { get }
    var conditions: WeatherContitionsModel { get }
}
