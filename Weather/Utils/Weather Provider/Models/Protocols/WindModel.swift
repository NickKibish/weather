//
//  WindModel.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol WindModel: Mappable {
    var speed: Measurement <UnitSpeed>? { get }
    var deg: Double? { get }
}
