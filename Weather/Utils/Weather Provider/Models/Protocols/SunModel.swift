//
//  SunModel.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol SunModel: Mappable {
    var sunrise: Date { get }
    var sunset: Date { get }
}
