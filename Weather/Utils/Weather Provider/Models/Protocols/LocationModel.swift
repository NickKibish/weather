//
//  LocationModel.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol LocationModel: Mappable {
    var countryCode: String { get }
    var cityName: String { get }
    var coordinates: CoordinatesModel { get }
}
