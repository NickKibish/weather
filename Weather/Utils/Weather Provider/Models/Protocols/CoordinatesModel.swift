//
//  CoordinatesModel.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation
import CoreLocation

protocol CoordinatesModel {
    var latitude: Double { get }
    var longitude: Double { get }
}

extension CLLocationCoordinate2D: CoordinatesModel { }
