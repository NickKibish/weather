//
//  WeatherContitionsModel.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol WeatherContitionsModel: Mappable {
    var sun: SunModel { get }
    var wind: WindModel? { get }
    var clouds: Int? { get }
    var visibility: Int? { get }
}
