//
//  MainWeatherDataModel.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol MainWeatherDataModel: Mappable {
    var currentTemp: Measurement <UnitTemperature> { get }
    var humidity: Int { get }
    var pressure: Measurement <UnitPressure> { get }
}
