//
//  Sun.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

struct Sun: SunModel {
    var sunrise: Date
    
    var sunset: Date
    
    init(json: [String : Any]?) throws {
        self.sunrise = Date()
        self.sunset = Date()
    }
    
    
}
