//
//  Weather.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

struct Weather: WeatherModel {
    var main: MainWeatherDataModel
    var wind: WindModel
    var conditions: WeatherContitionsModel
    var location: LocationModel
    
    init(json: [String : Any]?) throws {
        self.location = try Location(json: json)
        self.wind = try Wind(json: json!["wind"] as? [String : Any])
        self.main = try MainWeatherData(json: json)
        self.conditions = try WeatherContitions(json: json)
    }
}
