//
//  MainWeatherData.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

struct MainWeatherData: MainWeatherDataModel {
    var currentTemp: Measurement<UnitTemperature>
    var humidity: Int
    var pressure: Measurement<UnitPressure>
    
    init(json: [String : Any]?) throws {
        self.currentTemp = Measurement(value: 0, unit: UnitTemperature.kelvin)
        self.pressure = Measurement(value: 0, unit: UnitPressure.millibars)
        self.humidity = 0
    }
}
