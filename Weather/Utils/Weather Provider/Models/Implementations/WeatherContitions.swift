//
//  WeatherContitions.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

struct WeatherContitions: WeatherContitionsModel {
    var wind: WindModel?
    
    var clouds: Int?
    
    var visibility: Int?
    
    var sun: SunModel
    
    init(json: [String : Any]?) throws {
        self.sun = try Sun(json: json)
    }
    
}
