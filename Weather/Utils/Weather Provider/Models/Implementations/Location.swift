//
//  Location.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation
import CoreLocation

struct Location: LocationModel {
    var countryCode: String
    
    var cityName: String
    
    var coordinates: CoordinatesModel
    
    init(json: [String : Any]?) throws {
        countryCode = ""
        cityName = ""
        coordinates = CLLocationCoordinate2D(latitude: 0, longitude: 0)
    }
    
    
}
