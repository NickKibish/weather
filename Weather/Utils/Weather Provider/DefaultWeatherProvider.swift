//
//  DefaultWeatherProvider.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

enum WeatherProviderError: Error {
    case emptyData
}

struct DefaultWeatherProvider: WeatherProvider {
    private struct WeatherURLParameters {
        static let appID = "edc60874e635ded94b5ea2f4101774bc"
        static let id = "702550"
        static let baseURL = "http://api.openweathermap.org/data/2.5/weather"
    }
    
    private var networkManager: NetworkManager
    private var weatherURL : URLComponents {
        guard var components = URLComponents(string: WeatherURLParameters.baseURL) else { return URLComponents() }
        components.queryItems = [
            URLQueryItem(name: "APPID", value: WeatherURLParameters.appID),
            URLQueryItem(name: "id", value: WeatherURLParameters.id)
        ]
        return components
    }
    
    init(networkManager: NetworkManager = DefaultNetworkManager()) {
        self.networkManager = networkManager
    }
    
    func getWeather(in location: CoordinatesModel, success: @escaping WeatherProvider.SuccessCompletion, failure: @escaping WeatherProvider.FailureCompletion) {
        var urlComponents = weatherURL
        var queryItems = urlComponents.queryItems
        queryItems?.append(URLQueryItem(name: "lat", value: location.latitude.string))
        queryItems?.append(URLQueryItem(name: "lon", value: location.longitude.string))
        urlComponents.queryItems = queryItems
        
        networkManager.getRequest(url: urlComponents, succeed: { (responseData) in
            guard let json = responseData as? [String : Any] else {
                failure(WeatherProviderError.emptyData)
                return
            }
            
            do {
                success(try Weather(json: json))
            } catch let error {
                failure(error)
            }
        }, failure: failure)
    }
}
