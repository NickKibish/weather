//
//  WeatherProvider.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol WeatherProvider {
    typealias SuccessCompletion = (WeatherModel) -> Void
    typealias FailureCompletion = (Error) -> Void
    
    func getWeather(in location: CoordinatesModel, success: @escaping SuccessCompletion, failure: @escaping FailureCompletion)
}
