//
//  DefaultLocationProvider.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation
import CoreLocation

class DefaultLocationProvider: NSObject, LocationProvider {
    private let locationManager: CLLocationManager
    private (set) var lastLocation: CoordinatesModel?
    
    var locaionUpdateEvent: LocationProvider.LocationUpdateEvent
    
    func trackLocation() {
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
    }
    
    /// Hide default init
    private override init() {
        self.locationManager = CLLocationManager()
        self.locaionUpdateEvent = {(_, _) in }
    }
    
    /// Create location provider
    ///
    /// - Parameter locaionUpdateEvent: inject location update event
    init(locationManager: CLLocationManager = CLLocationManager(), locaionUpdateEvent: @escaping LocationUpdateEvent) {
        self.locationManager = locationManager
        self.locaionUpdateEvent = locaionUpdateEvent
        super.init()
        locationManager.delegate = self
    }
    
    private func getRegionLocation() {
        
    }
}

extension DefaultLocationProvider: CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        lastLocation = location.coordinate
        self.locaionUpdateEvent(location.coordinate, .currentLocation)
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        self.locaionUpdateEvent(lastLocation, .notDetermined)
    }
}
