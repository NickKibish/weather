//
//  LocationProvider.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

/// This enum describes accuracy lever of user's location
///
/// - currentLocation: Current location determined with CLLocationManager
/// - region: Region Location determined with CLRegion
/// - notDetermined: Cannot determin any location
enum LocationAccuracy {
    case currentLocation, region, notDetermined
}

/// Protocol provides methods for getting user's location
protocol LocationProvider {
    /// Describe completion block for user's location
    typealias LocationUpdateEvent = (_ coordinates: CoordinatesModel?, _ accuracy: LocationAccuracy) -> Void
    var locaionUpdateEvent: LocationUpdateEvent { get }
    func trackLocation()
}
