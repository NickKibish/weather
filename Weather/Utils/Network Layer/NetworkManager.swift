//
//  NetworkManager.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

enum HTTPMethod: String {
    case get = "GET"
}

enum NetworkError: Error {
    case wrongURL
}

protocol NetworkManager {
    typealias SucceedCallback = (Any?) -> Void
    typealias FailureCallback = (Error) -> Void
    
    func getRequest(url: URLConvertable, succeed: @escaping SucceedCallback, failure: @escaping FailureCallback)
}
