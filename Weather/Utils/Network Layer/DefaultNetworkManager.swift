//
//  DefaultNetworkManager.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

struct DefaultNetworkManager: NetworkManager {
    func getRequest(url: URLConvertable, succeed: @escaping NetworkManager.SucceedCallback, failure: @escaping NetworkManager.FailureCallback) {
        guard let requestURL = url.url else {
            failure(NetworkError.wrongURL)
            return
        }
        let session = URLSession(configuration: .default)
        let task = session.dataTask(with: requestURL) { (data, response, error) in
            if let responseError = error {
                failure(responseError)
                return
            }
            
            guard let responseData = data else {
                succeed(nil)
                return
            }
            
            do {
                let json = try JSONSerialization.jsonObject(with: responseData, options: [])
                succeed(json)
            } catch let error {
                failure(error)
            }
        }
        task.resume()
    }
}
