//
//  URLConvertable.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

protocol URLConvertable {
    var url: URL? { get }
}

extension URL: URLConvertable {
    var url: URL? {
        return self
    }
}

extension String: URLConvertable {
    var url: URL? {
        return URL(string: self)
    }
}

extension URLComponents: URLConvertable { }
