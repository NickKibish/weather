//
//  Double+Ext.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

extension Double {
    var string: String {
        return String(format: "%f", self)
    }
}
