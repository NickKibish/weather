//
//  JSONSerialization+Ext.swift
//  Weather
//
//  Created by EPAM on 3/29/18.
//  Copyright © 2018 EPAM. All rights reserved.
//

import Foundation

enum JSONSerializationError: Error {
    case castingError
}

extension JSONSerialization {
    static func object<T>(with data: Data, options: JSONSerialization.ReadingOptions = []) throws -> T {
        guard let json = try self.jsonObject(with: data, options: options) as? T else {
            throw JSONSerializationError.castingError
        }
        return json
    }
}
